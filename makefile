PROJ = file-utilities

IDIRS = include vendor
SRCDIR = src

BUILDDIR = build
OBJDIR = $(BUILDDIR)/obj

_LIBDIRS = lib
LIBDIRS = $(foreach LIBDIR,$(_LIBDIRS),-L$(LIBDIR) -Wl,-rpath-link,$(LIBDIR))

CC = clang
CFLAGS = $(foreach DIR,$(IDIRS),-I$(DIR)) -fPIC
DBFLAGS = -g -D_DEBUG -O0 -Wall
RELFLAGS = -O3
SHAREDFLAGS = -shared

_LIBS = cjson frozen
LIBS = $(foreach LIB,$(_LIBS),-l$(LIB))

DEPS = $(shell find include -name *.h)
VENDORDEPS = $(shell find vendor -name *.h)

OBJ = $(patsubst $(SRCDIR)/%.c,$(OBJDIR)/%.o,$(shell find $(SRCDIR) -name *.c))

$(OBJDIR)/%.o : $(SRCDIR)/%.c $(DEPS)
	$(CC) $(CFLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) -c -o $@ $<

.PHONY: build

build : $(OBJ)
	$(CC) $(CFLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) $(LIBDIRS) $(SHAREDFLAGS) -o $(BUILDDIR)/lib$(PROJ).so $^ $(LIBS)

.PHONY: clean

clean:
	rm -rf release
	rm -rf $(BUILDDIR)
	mkdir -p $(OBJDIR)

.PHONY: release

release : clean
	@make$(if $(WINDOWS), windows,) RELEASE=yes

.PHONY: rebuild

rebuild : clean build

WINCC = x86_64-w64-mingw32-clang
WINCFLAGS = $(foreach DIR,$(IDIRS),-I$(DIR))
WINFLAGS = -I/usr/local/include -Wl,-subsystem,windows
_WINLIBS = cjson frozen
WINLIBS = $(foreach LIB,$(_WINLIBS),-l$(LIB))

_WINLIBDIRS = winlib
WINLIBDIRS = $(foreach LIBDIR,$(_WINLIBDIRS),-L$(LIBDIR))

WINOBJ = $(patsubst %.o,%.obj,$(OBJ))

$(OBJDIR)/%.obj : $(SRCDIR)/%.c $(DEPS)
	$(WINCC) $(WINCFLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) $(WINFLAGS) -c -o $@ $<

.PHONY: windows

windows : $(WINOBJ)
	$(WINCC) $(WINCFLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) $(WINFLAGS) $(WINLIBDIRS) $(SHAREDFLAGS) -o $(BUILDDIR)/$(PROJ).dll $^ $(WINLIBS)
	cp winlib/* $(BUILDDIR)/
