#pragma once

#define internal static

typedef unsigned long long int64;
typedef int int32;
typedef short int16;
typedef char int8;

typedef unsigned long long uint64;
typedef unsigned int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;

typedef double real64;
typedef float real32;

typedef unsigned char bool;
#define true 1
#define false 0

#define UUID_LENGTH 63
typedef struct uuid_t {
	char string[UUID_LENGTH + 1];
} UUID;