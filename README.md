# File Utilities

This is a [shared library](https://en.wikipedia.org/wiki/Shared_library) containing common file management functions used by the **[Ghoti](https://gitlab.com/psahani/ghoti)** game engine and its associated projects.

## Building

### Linux

Since care has been taken to include redistributable binaries of all necessary libraries in the `lib/` folder, all that should be needed to build the engine is to have [Clang](https://clang.llvm.org/) and [Make](https://www.gnu.org/software/make/) installed before running this single command:

```sh
$ make rebuild
```

The resulting shared library will be placed in the `build/` folder by default.

Release builds are possible through a makefile target:

```sh
$ make release
```

### Windows

Building for Windows is only supported through [cross-compilation](https://en.wikipedia.org/wiki/Cross_compiler) from a Linux host using the [MinGW](https://www.mingw-w64.org/) compatibility environment. Redistributables are located in the `winlib/` folder. Assuming the `x86_64-w64-mingw32-clang` binary is available on the [system path](https://en.wikipedia.org/wiki/PATH_(variable)), simply run this command:

```sh
$ make windows
```

Release builds are also possible by setting the appropriate [environment variable](https://en.wikipedia.org/wiki/Environment_variable):

```sh
$ WINDOWS=true make release
```