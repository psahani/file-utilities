#include "file_utilities.h"
#include "log.h"

#include <frozen/frozen.h>

#include <sys/stat.h>

#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>

internal void formatJSON(const char *filename, Log log);
internal void formatJSONArrays(const char *filename, Log log);

UUID generateUUID(void) {
	UUID uuid = {};

	for (uint32 i = 0; i < UUID_LENGTH; i++) {
		do {
			uuid.string[i] = (
				rand() % (MAX_UUID_ASCII - MIN_UUID_ASCII)
			) + MIN_UUID_ASCII;
		} while (
			uuid.string[i] == '\\' ||
			uuid.string[i] == '[' ||
			uuid.string[i] == ']'
		);
	}

	return uuid;
}

UUID stringToUUID(const char *string) {
	UUID uuid = {};

	if (string) {
		strcpy(uuid.string, string);
	}

	return uuid;
}

char* concatenateStrings(
	const char *stringA,
	const char *separator,
	const char *stringB
) {
	if (!stringA) {
		return NULL;
	}

	uint32 numExtraCharacters = 1;

	if (separator) {
		numExtraCharacters += strlen(separator);
	}

	if (stringB) {
		numExtraCharacters += strlen(stringB);
	}

	char *string = malloc(strlen(stringA) + numExtraCharacters);

	if (separator && stringB) {
		sprintf(string, "%s%s%s", stringA, separator, stringB);
	} else if (separator) {
		sprintf(string, "%s%s", stringA, separator);
	} else if (stringB) {
		sprintf(string, "%s%s", stringA, stringB);
	} else {
		sprintf(string, "%s", stringA);
	}

	return string;
}

char* getFullFilePath(
	const char *filename,
	const char *extension,
	const char *folder
) {
	if (!filename) {
		return NULL;
	}

	uint32 numExtraCharacters = 1;

	if (extension) {
		numExtraCharacters += strlen(extension) + 1;
	}

	if (folder) {
		numExtraCharacters += strlen(folder) + 1;
	}

	char *fullFilename = malloc(strlen(filename) + numExtraCharacters);

	if (extension && folder) {
		sprintf(fullFilename, "%s/%s.%s", folder, filename, extension);
	} else if (extension) {
		sprintf(fullFilename, "%s.%s", filename, extension);
	} else if (folder) {
		sprintf(fullFilename, "%s/%s", folder, filename);
	} else {
		sprintf(fullFilename, "%s", filename);
	}

	return fullFilename;
}

char* getPrefix(const char *string, const char *delimiter) {
	const char *delimiterLocation = strstr(string, delimiter);
	if (delimiterLocation) {
		uint32 prefixLength = delimiterLocation - string;

		char *prefix = calloc(prefixLength + 1, 1);
		memcpy(prefix, string, prefixLength);

		return prefix;
	}

	return NULL;
}

char* getSuffix(const char *string, const char *delimiter) {
	const char *delimiterLocation = strstr(string, delimiter);
	if (delimiterLocation) {
		delimiterLocation += strlen(delimiter);

		const char *stringEnd = string + strlen(string);
		uint32 suffixLength = stringEnd - delimiterLocation;

		char *suffix = calloc(suffixLength + 1, 1);
		memcpy(suffix, delimiterLocation, suffixLength);

		return suffix;
	}

	return NULL;
}

char* getParentFolder(const char *filePath) {
	char *backslash = strrchr(filePath, '/');
	if (backslash) {
		uint32 folderLength = backslash - filePath;
		char *parentFolder = calloc(1, folderLength + 1);
		memcpy(parentFolder, filePath, folderLength);
		return parentFolder;
	}

	return NULL;
}

char* getFilename(const char *filePath) {
	char *filename = NULL;

	char *backslash = strrchr(filePath, '/');
	if (backslash) {
		uint32 length = filePath + strlen(filePath) - backslash;
		filename = malloc(length);
		strcpy(filename, backslash + 1);
	} else {
		filename = malloc(strlen(filePath) + 1);
		strcpy(filename, filePath);
	}

	return filename;
}

char* getExtension(const char *filename) {
	const char *a = strrchr(filename, '.');
	if (a) {
		const char *b = filename + strlen(filename);

		char *extension = malloc(b - a);
		memcpy(extension, a + 1, b - a);
		return extension;
	}

	return NULL;
}

char* removeExtension(const char *filename) {
	const char *extension = strrchr(filename, '.');

	char *name = malloc(extension - filename + 1);
	memcpy(name, filename, extension - filename);
	name[extension - filename] = '\0';

	return name;
}

void writeString(const char *string, FILE *file) {
	uint32 size = strlen(string) + 1;
	fwrite(&size, sizeof(uint32), 1, file);
	fwrite(string, size, 1, file);
}

void writeUUID(UUID uuid, FILE *file) {
	fwrite(uuid.string, sizeof(UUID), 1, file);
}

void writeStringAsUUID(const char *string, FILE *file) {
	writeUUID(stringToUUID(string), file);
}

void writeCharacter(char character, uint32 n, FILE *file) {
	for (uint32 i = 0; i < n; i++) {
		fwrite(&character, sizeof(char), 1, file);
	}
}

char* readString(FILE *file) {
	uint32 stringLength;
	fread(&stringLength, sizeof(uint32), 1, file);

	char *string = malloc(stringLength);
	fread(string, stringLength, 1, file);

	return string;
}

UUID readUUID(FILE *file) {
	UUID uuid = {};
	fread(uuid.string, sizeof(UUID), 1, file);
	return uuid;
}

UUID readStringAsUUID(FILE *file) {
	char *string = readString(file);
	UUID uuid = stringToUUID(string);
	free(string);

	return uuid;
}

char* readFile(const char *filename, uint64 *fileLength, Log log) {
	char *fileBuffer = NULL;
	*fileLength = 0;

	FILE *file = fopen(filename, "rb");

	if (file) {
		fseek(file, 0, SEEK_END);
		*fileLength = ftell(file);
		fseek(file, 0, SEEK_SET);
		fileBuffer = calloc(*fileLength + 1, 1);

		if (fileBuffer) {
			fread(fileBuffer, 1, *fileLength, file);
		}

		fclose(file);
	} else {
		LOG(log, "Failed to open %s\n", filename);
	}

	return fileBuffer;
}

void copyFile(const char *filename, const char *destination, Log log) {
	uint64 fileLength;
	char *fileBuffer = readFile(filename, &fileLength, log);

	if (fileBuffer) {
		FILE *file = fopen(destination, "wb");
		fwrite(fileBuffer, fileLength, 1, file);
		fclose(file);
		free(fileBuffer);
	}
}

void renameFile(
	const char *filename,
	const char *extension,
	const char *folder,
	const char *newFilename,
	const char *newExtension,
	Log log
) {
	char *fullFilename = getFullFilePath(filename, extension, folder);
	char *newFullFilename = getFullFilePath(newFilename, newExtension, folder);

	copyFile(fullFilename, newFullFilename, log);
	remove(fullFilename);

	free(fullFilename);
	free(newFullFilename);
}

int32 copyFolder(const char *folder, const char *destination, Log log) {
	MKDIR(destination);

	DIR *dir = opendir(folder);
	if (dir) {
		struct dirent *dirEntry = readdir(dir);
		while (dirEntry) {
			if (strcmp(dirEntry->d_name, ".") &&
				strcmp(dirEntry->d_name, "..")) {
				char *folderPath = getFullFilePath(
					dirEntry->d_name,
					NULL,
					folder
				);

				char *destinationFolderPath = getFullFilePath(
					dirEntry->d_name,
					NULL,
					destination
				);

				struct stat info;
				stat(folderPath, &info);

				if (S_ISDIR(info.st_mode)) {
					if (copyFolder(
						folderPath,
						destinationFolderPath,
						log) == -1) {
						free(folderPath);
						free(destinationFolderPath);
						closedir(dir);
						return -1;
					}
				} else if (S_ISREG(info.st_mode)) {
					copyFile(folderPath, destinationFolderPath, log);
				}

				free(folderPath);
				free(destinationFolderPath);
			}

			dirEntry = readdir(dir);
		}

		closedir(dir);
	} else {
		LOG(log, "Failed to open %s\n", folder);
		return -1;
	}

	return 0;
}

int32 deleteFolder(const char *folder, bool errors, Log log) {
	DIR *dir = opendir(folder);
	if (dir) {
		struct dirent *dirEntry = readdir(dir);
		while (dirEntry) {
			if (strcmp(dirEntry->d_name, ".") &&
				strcmp(dirEntry->d_name, "..")) {
				char *folderPath = getFullFilePath(
					dirEntry->d_name,
					NULL,
					folder);

				struct stat info;
				stat(folderPath, &info);

				if (S_ISDIR(info.st_mode)) {
					if (deleteFolder(folderPath, errors, log) == -1) {
						free(folderPath);
						closedir(dir);
						return -1;
					}
				} else if (S_ISREG(info.st_mode)) {
					remove(folderPath);
				}

				free(folderPath);
			}

			dirEntry = readdir(dir);
		}

		closedir(dir);
	} else {
		if (errors) {
			LOG(log, "Failed to open %s\n", folder);
		}

		return -1;
	}

	rmdir(folder);

	return 0;
}

cJSON* loadJSON(const char *filename, Log log) {
	cJSON *json = NULL;

	char *jsonFilename = getFullFilePath(filename, "json", NULL);

	uint64 fileLength;
	char *fileBuffer = readFile(jsonFilename, &fileLength, log);

	free(jsonFilename);

	if (fileBuffer) {
		json = cJSON_Parse(fileBuffer);
		free(fileBuffer);
	}

	return json;
}

uint32 getJSONListSize(const cJSON *list) {
	uint32 size = 0;

	cJSON *item = list->child;
	while (item) {
		size++;
		item = item->next;
	}

	return size;
}

void writeJSON(
	const cJSON *json,
	const char *filename,
	bool formatted,
	Log log
) {
	char *jsonFilename = getFullFilePath(filename, "json", NULL);
	FILE *file = fopen(jsonFilename, "wb");

	char *jsonString = cJSON_Print(json);
	fwrite(jsonString, strlen(jsonString), 1, file);
	free(jsonString);

	fclose(file);

	json_prettify_file(jsonFilename);

	if (formatted) {
		formatJSON(jsonFilename, log);
	}

	free(jsonFilename);
}

void formatJSON(const char *filename, Log log) {
	uint64 fileLength;
	char *fileBuffer = readFile(filename, &fileLength, log);

	FILE *file = fopen(filename, "wb");

	uint32 numSpaces;
	uint32 currentIndent;
	uint64 j;

	for (uint64 i = 0; i < fileLength; i++) {
		char character = fileBuffer[i];

		switch (character) {
			case '{':
			case '[':
				if (fileBuffer[i + 1] == '}') {
					fseek(file, -1, SEEK_CUR);
					writeCharacter('\n', 1, file);
					writeCharacter('\t', currentIndent, file);
					writeCharacter(character, 1, file);
					writeCharacter('\n', 1, file);
					writeCharacter('\t', currentIndent + 1, file);
					writeCharacter('\n', 1, file);
					writeCharacter('\t', currentIndent, file);
					character = fileBuffer[++i];
				} else if (i >= 2 &&
					fileBuffer[i - 2] == ':' &&
					fileBuffer[i + 1] != '}') {
					fseek(file, -1, SEEK_CUR);
					writeCharacter('\n', 1, file);
					writeCharacter('\t', currentIndent, file);
				}

				break;
			default:
				break;
		}

		writeCharacter(character, 1, file);

		switch (character) {
			case '\n':
				if (i - 1 > 0 &&
					fileBuffer[i - 1] == ',' &&
					i - 2 > 0 &&
					(fileBuffer[i - 2] == '}' || fileBuffer[i - 2] == ']')) {
					writeCharacter('\n', 1, file);
				} else {
					for (
						j = i + 1;
						j < fileLength && fileBuffer[j] != '\n';
						j++
					);

					if ((fileBuffer[--j] == '{' ||
						(fileBuffer[j - 1] == '{' && fileBuffer[j] == '}')) &&
						fileBuffer[i - 1] != '{' &&
						fileBuffer[i - 1] != '[') {
						writeCharacter('\n', 1, file);
					}
				}

				numSpaces = 0;
				for (j = i + 1; fileBuffer[j] == ' '; j++, i++) {
					numSpaces++;
				}

				currentIndent = numSpaces / 2;
				writeCharacter('\t', currentIndent, file);

				break;
			default:
				break;
		}
	}

	fclose(file);
	free(fileBuffer);

	formatJSONArrays(filename, log);
}

void formatJSONArrays(const char *filename, Log log) {
	uint64 fileLength;
	char *fileBuffer = readFile(filename, &fileLength, log);

	FILE *file = fopen(filename, "wb");

	uint64 j;
	uint32 k;
	uint32 currentLineLength;
	char *array;
	uint64 arrayStart;
	uint32 arrayLength;
	uint32 formattedArrayLength;
	bool formatArray;
	char arrayCharacter;
	uint32 currentIndent;

	for (uint64 i = 0; i < fileLength; i++) {
		char character = fileBuffer[i];

		writeCharacter(character, 1, file);

		if (character == '\t') {
			currentLineLength += 4;
		} else {
			currentLineLength++;
		}

		switch (character) {
			case '\n':
				currentLineLength = 0;
				break;
			case ':':
				for (
					j = i + 1;
					fileBuffer[j] == '\n' || fileBuffer[j] == '\t';
					j++
				);

				if (fileBuffer[j] == '[') {
					arrayStart = j;

					formatArray = true;
					for (++j; fileBuffer[j] != ']'; j++) {
						if (fileBuffer[j] == '{' || fileBuffer[j] == '[') {
							formatArray = false;
							break;
						}
					}

					if (formatArray) {
						arrayLength = ++j - arrayStart;

						if (arrayLength > 2) {
							array = malloc(arrayLength);
							memcpy(array, &fileBuffer[arrayStart], arrayLength);

							formattedArrayLength = 0;
							for (j = 0; j < arrayLength; j++) {
								arrayCharacter = array[j];
								if (arrayCharacter != '\t') {
									formattedArrayLength++;
								}

								if (arrayCharacter == ',') {
									formattedArrayLength--;
								}
							}

							if (fileBuffer[arrayStart + arrayLength] == ',') {
								formattedArrayLength++;
							}

							if (++formattedArrayLength + currentLineLength
								< MAX_JSON_LINE_LENGTH) {
								writeCharacter(' ', 1, file);

								for (j = 0; j < arrayLength; j++) {
									arrayCharacter = array[j];

									if (arrayCharacter == '\n') {
										if (array[j - 1] == '[') {
											continue;
										}

										for (k = j + 1; array[k] == '\t'; k++);
										if (array[k] == ']') {
											continue;
										}

										arrayCharacter = ' ';
									} else if (arrayCharacter == '\t') {
										continue;
									}

									writeCharacter(arrayCharacter, 1, file);
								}

								character = fileBuffer[
									i = arrayStart + arrayLength - 1
								];

								if (fileBuffer[i + 1] == ',') {
									writeCharacter(',', 1, file);

									for (k = 0, j = i + 2; k < 3; j++) {
										if (fileBuffer[j] == '\n') {
											k++;
										}
									}

									for (++j; fileBuffer[j] == '\t'; j++);

									i++;
									if (fileBuffer[j] != '{') {
										i++;
									}
								}
							}

							free(array);
						} else {
							for (currentIndent = 0, ++i; i <= arrayStart; i++) {
								character = fileBuffer[i];
								if (character == '\t') {
									currentIndent++;
								}

								writeCharacter(character, 1, file);
							}

							writeCharacter('\n', 1, file);
							writeCharacter('\t', currentIndent + 1, file);
							writeCharacter('\n', 1, file);
							writeCharacter('\t', currentIndent, file);

							i--;
						}
					}
				}

				break;
			default:
				break;
		}
	}

	fclose(file);
	free(fileBuffer);
}