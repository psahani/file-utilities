#include "entity_utilities.h"

#include <stdio.h>

void addInteger(
	cJSON *component,
	const char *name,
	const char *dataType,
	int64 integer
) {
	cJSON *object = cJSON_CreateObject();
	cJSON_AddStringToObject(object, "name", name);
	cJSON_AddNumberToObject(object, dataType, integer);
	cJSON_AddItemToArray(component, object);
}

void addRealNumber(
	cJSON *component,
	const char *name,
	const char *dataType,
	real64 number
) {
	cJSON *object = cJSON_CreateObject();
	cJSON_AddStringToObject(object, "name", name);
	cJSON_AddNumberToObject(object, dataType, number);
	cJSON_AddItemToArray(component, object);
}

void addFloatArray(
	cJSON *component,
	const char *name,
	uint32 numValues,
	const real32 *values
) {
	cJSON *object = cJSON_CreateObject();

	cJSON_AddStringToObject(object, "name", name);
	cJSON_AddItemToObject(
		object,
		"float32",
		cJSON_CreateFloatArray(values, numValues)
	);

	cJSON_AddItemToArray(component, object);
}

void addString(
	cJSON *component,
	const char *name,
	uint32 length,
	const char *string
) {
	cJSON *object = cJSON_CreateObject();
	cJSON_AddStringToObject(object, "name", name);

	char dataType[1024];
	sprintf(dataType, "char(%d)", length);
	cJSON_AddStringToObject(object, dataType, string);

	cJSON_AddItemToArray(component, object);
}

void addBool(cJSON *component, const char *name, bool value) {
	cJSON *object = cJSON_CreateObject();
	cJSON_AddStringToObject(object, "name", name);
	cJSON_AddBoolToObject(object, "bool", value);
	cJSON_AddItemToArray(component, object);
}

void addUUID(cJSON *component, const char *name, UUID uuid) {
	cJSON *object = cJSON_CreateObject();
	cJSON_AddStringToObject(object, "name", name);
	cJSON_AddStringToObject(object, "uuid", uuid.string);
	cJSON_AddItemToArray(component, object);
}

void addTransform(
	cJSON *components,
	const real32 *positionValues,
	const real32 *rotationValues,
	const real32 *scaleValues,
	UUID *parent,
	UUID *firstChild,
	UUID *nextSibling
) {
	cJSON *transform = cJSON_AddArrayToObject(components, "transform");

	real32 defaultPositionRotation[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	real32 defaultScale[] = { 1.0f, 1.0f, 1.0f };

	addFloatArray(
		transform,
		"position",
		3,
		positionValues ? positionValues : defaultPositionRotation
	);

	addFloatArray(
		transform,
		"rotation",
		4,
		rotationValues ? rotationValues : defaultPositionRotation
	);

	addFloatArray(
		transform,
		"scale",
		3,
		scaleValues ? scaleValues : defaultScale
	);

	UUID emptyUUID = {};

	addUUID(transform, "parent", parent ? *parent : emptyUUID);
	addUUID(transform, "first child", firstChild ? *firstChild : emptyUUID);
	addUUID(transform, "next sibling", nextSibling ? *nextSibling : emptyUUID);
	addBool(transform, "dirty", true);

	addFloatArray(transform, "global position", 3, defaultPositionRotation);
	addFloatArray(transform, "global rotation", 4, defaultPositionRotation);
	addFloatArray(transform, "global scale", 3, defaultScale);
	addFloatArray(
		transform,
		"last global position",
		3,
		defaultPositionRotation
	);

	addFloatArray(
		transform,
		"last global rotation",
		4,
		defaultPositionRotation
	);

	addFloatArray(transform, "last global scale", 3, defaultScale);
}

void getFloatArray(cJSON *array, uint32 numValues, real32 *values) {
	if (array) {
		if (cJSON_IsArray(array)) {
			array = array->child;
			for (uint32 i = 0; array; i++) {
				values[i] = (real32)array->valuedouble;
				array = array->next;
			}
		} else {
			for (uint32 i = 0; i < numValues; i++) {
				values[i] = (real32)array->valuedouble;
			}
		}
	}
}